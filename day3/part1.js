const fs = require('fs');

const input = fs.readFileSync('input.txt', 'utf-8').split('\n');
const wire1Commands = input[0].split(',');
const wire2Commands = input[1].split(',');

function generateCoordinateList(commands) {
    let current = { x: 0, y: 0 };
    const coordinates = [];
    commands.forEach(cmd => {
        for (let i = 0; i < parseInt(cmd.slice(1), 10); i++) {
            if (cmd[0] == 'U') coordinates.push(current = { x: current.x, y: current.y + 1 });
            if (cmd[0] == 'D') coordinates.push(current = { x: current.x, y: current.y - 1 });
            if (cmd[0] == 'R') coordinates.push(current = { x: current.x + 1, y: current.y });
            if (cmd[0] == 'L') coordinates.push(current = { x: current.x - 1, y: current.y });
        }
    });
    return coordinates.map(coord => coord.x + '|' + coord.y);
}

const wire1Coords = new Set(generateCoordinateList(wire1Commands));
const wire2Coords = new Set(generateCoordinateList(wire2Commands));

const intersect = [ ...wire1Coords ].filter(c => wire2Coords.has(c));
const bestDistance = intersect.map(xy => {
    const [ x, y ] = xy.split('|').map(num => Math.abs(parseInt(num, 10)));
    return x + y;
}).reduce((a, b) => a < b ? a : b, Infinity);

console.log('Best manhattan distance is: ', bestDistance);
