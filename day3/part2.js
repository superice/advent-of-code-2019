const fs = require('fs');

const input = fs.readFileSync('input.txt', 'utf-8').split('\n');
const wire1Commands = input[0].split(',');
const wire2Commands = input[1].split(',');

function generateCoordinateList(commands) {
    console.log('Start generating coordinates');
    let current = { x: 0, y: 0 };
    const coordinates = [];
    commands.forEach((cmd, idx) => {
        for (let i = 0; i < parseInt(cmd.slice(1), 10); i++) {
            if (cmd[0] == 'U') coordinates.push(current = { x: current.x, y: current.y + 1 });
            if (cmd[0] == 'D') coordinates.push(current = { x: current.x, y: current.y - 1 });
            if (cmd[0] == 'R') coordinates.push(current = { x: current.x + 1, y: current.y });
            if (cmd[0] == 'L') coordinates.push(current = { x: current.x - 1, y: current.y });
        }
    });
    console.log('Generated ', coordinates.length ,' coordinates')
    return coordinates.map((coord, idx) => ({ ...coord, steps: (idx + 1) }));
}

function reduceToMap(coords) {
    const map = {};
    for (let i = 0; i < coords.length; i++) {
        const coord = coords[i];
        const key = coord.x + '|' + coord.y;
        map[key] = Math.min(map[key] || Infinity, coord.steps);
    }
    return map;
}

const wire1Coords = reduceToMap(generateCoordinateList(wire1Commands));
const wire2Coords = reduceToMap(generateCoordinateList(wire2Commands))

console.log('Calculating intersections');
const intersect = Object.keys(wire1Coords).filter(coord => wire2Coords[coord] != null);
const bestDistance = intersect.map(xy => {
    return wire1Coords[xy] + wire2Coords[xy];
}).reduce((a, b) => a < b ? a : b, Infinity);

console.log('Min number of steps is: ', bestDistance);
