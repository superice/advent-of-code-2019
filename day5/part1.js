const fs = require('fs');
const prompt = require('prompt');

let program = [ 99 ];

function initializeProgram(noun, verb) {
    program = fs.readFileSync('./input.txt', 'utf-8').split(',')
        .filter(x => x)
        .map(x => parseInt(x, 10));
    if (noun != null) program[1] = noun;
    if (verb != null) program[2] = verb;
}

function processOpcode(opcodePlusModes, ...args) {
    const opcode = parseInt((opcodePlusModes + '').slice(-2), 10);
    if (opcode == 1) {
        const { opcode, modes } = processOpcodePlusMode(opcodePlusModes, 3);
        add(...processArgumentList(modes, args));
        return 4;
    } else if (opcode == 2) {
        const { opcode, modes } = processOpcodePlusMode(opcodePlusModes, 3);
        multiply(...processArgumentList(modes, args));
        return 4;
    } else if (opcode == 3) {
        const { opcode, modes } = processOpcodePlusMode(opcodePlusModes, 1);
        return new Promise(resolve => {
            prompt.start();
            prompt.get([ 'input' ], function(err, result) {
                if (err) {
                    console.error(err);
                    resolve(-1);
                } else {
                    program[args[0]] = parseInt(result, 10);
                    resolve(2);
                }
            });
        });
    } else if (opcode == 4) {
        const { opcode, modes } = processOpcodePlusMode(opcodePlusModes, 1);
        console.log(program[processArgument(modes[0], args[0])]);
        return 2;
    } else if (opcode == 99) {
        return -1;
    } else {
        console.error('Something went wrong, opcode', opcode, ', args: ', args);
        return -1;
    }
}

function processOpcodePlusMode(opcodePlusModes, numOfArgs) {
    const fullOpcodeModes = (opcodePlusModes + '').padStart(2 + numOfArgs, '0');
    const opcode = parseInt(fullOpcodeModes.slice(numOfArgs), 10);
    const modes = fullOpcodeModes.slice(0, numOfArgs).split('').reverse().map(p => parseInt(p, 10));
    return { opcode, modes };
}

function processArgumentList(modes, args) {
    const output = [];
    for (let i = 0; i < args.length; i++) {
        output.push(processArgument(modes[i], args[i]));
    }
    return output;
}

function processArgument(mode, arg) {
    if (mode == 0) {
        return program[arg];
    } else if (mode == 1) {
        return arg;
    }
}

function add(arg1, arg2, target) {
    program[target] = program[arg1] + program[arg2];
}

function multiply(arg1, arg2, target) {
    program[target] = program[arg1] * program[arg2];
}

async function runProgram() {
    let pointer = 0;
    let increasePointerWith = 0;
    do {
        console.log('Executing', pointer, increasePointerWith);
        increasePointerWith = await processOpcode(program[pointer], program[pointer + 1], program[pointer + 2], program[pointer + 3]);
        pointer += increasePointerWith;
    } while (increasePointerWith > 0);
}

initializeProgram();
runProgram();
