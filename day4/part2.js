function testPassword(password) {
	let seenDouble = false;
	let highestDigit = 0;
    let streak = 0;
	for (let i = 0; i < password.length; i++) {
		if (parseInt(password[i], 10) < highestDigit) return false;
		highestDigit = parseInt(password[i], 10);
		if (i > 0) {
            if (password[i - 1] == password[i]) {
                streak++;
            } else {
                if (streak == 1) seenDouble = true;
                streak = 0;
            }
        }
	}
	return seenDouble || streak == 1;
}

function testRange(start, end) {
	let matches = 0;
	for (let password = start; password <= end; password++) {
		if (testPassword('' + password)) matches++;
		if (password % 1000 == 0) console.log('Matches so far: ', matches, ' - percentage: ', Math.round((password - start) / (end - start) * 100) + '%');
	}
	return matches;
}

console.log('Matches in range: ' + testRange(183564, 657474));
