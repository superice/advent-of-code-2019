const fs = require('fs');

function calculation(mass) {
	const fuel = Math.max(Math.floor(mass / 3) - 2, 0);
	if (fuel == 0) return fuel;
	return fuel + calculation(fuel);
}

const solution = fs.readFileSync('input.txt', 'utf-8')
		.split('\n')
		.filter(str => str)
		.map(str => parseInt(str, 10))
		.map(calculation)
		.reduce((total, part) => total + part, 0);

console.log('Total fuel required is:', solution);
	
