const fs = require('fs');

function calculation(mass) {
	return Math.floor(mass / 3) - 2;
}

const solution = fs.readFileSync('input.txt', 'utf-8')
		.split('\n')
		.filter(str => str)
		.map(str => parseInt(str, 10))
		.map(calculation)
		.reduce((total, part) => total + part, 0);

console.log('Total fuel required is:', solution);
	
