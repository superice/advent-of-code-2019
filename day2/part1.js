const fs = require('fs');

const program = fs.readFileSync('./input.txt', 'utf-8').split(',')
    .filter(x => x)
    .map(x => parseInt(x, 10));

function processOpcode(opcode, ...args) {
    if (opcode == 1) {
        add(...args);
        return false;
    } else if (opcode == 2) {
        multiply(...args);
        return false;
    } else if (opcode == 99) {
        return true;
    } else {
        console.error('Something went wrong, opcode', opcode, ', args: ', args);
        return true;
    }
}

function add(arg1, arg2, target) {
    program[target] = program[arg1] + program[arg2];
}

function multiply(arg1, arg2, target) {
    program[target] = program[arg1] * program[arg2];
}

function runProgram() {
    let pointer = 0;
    while (!processOpcode(program[pointer], program[pointer + 1], program[pointer + 2], program[pointer + 3])) {
        pointer += 4;
    }
}

function restorePrescribedState() {
    program[1] = 12;
    program[2] = 2;
}

restorePrescribedState();
runProgram();
// console.log('Program: ', program);
console.log('Output of position 0: ', program[0]);
