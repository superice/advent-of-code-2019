const fs = require('fs');

let program = [ 99 ];

function initializeProgram(noun, verb) {
    program = fs.readFileSync('./input.txt', 'utf-8').split(',')
        .filter(x => x)
        .map(x => parseInt(x, 10));
    if (noun != null) program[1] = noun;
    if (verb != null) program[2] = verb;
}

function processOpcode(opcode, ...args) {
    if (opcode == 1) {
        add(...args);
        return false;
    } else if (opcode == 2) {
        multiply(...args);
        return false;
    } else if (opcode == 99) {
        return true;
    } else {
        console.error('Something went wrong, opcode', opcode, ', args: ', args);
        return true;
    }
}

function add(arg1, arg2, target) {
    program[target] = program[arg1] + program[arg2];
}

function multiply(arg1, arg2, target) {
    program[target] = program[arg1] * program[arg2];
}

function runProgram() {
    let pointer = 0;
    while (!processOpcode(program[pointer], program[pointer + 1], program[pointer + 2], program[pointer + 3])) {
        pointer += 4;
    }
}

function tryAndFindDesiredOutput() {
    for (let noun = 0; noun <= 99; noun++) {
        for (let verb = 0; verb <= 99; verb++) {
            initializeProgram(noun, verb);
            runProgram();
            if (program[0] == 19690720) {
                console.log('Noun: ', noun);
                console.log('Verb: ', verb);
                console.log('100 * noun + verb: ' + (100 * noun + verb));
                return;
            }
        }
    }
    console.log('Could not find params to satisfy output');
}

tryAndFindDesiredOutput();
// console.log('Program: ', program);
// console.log('Output of position 0: ', program[0]);
